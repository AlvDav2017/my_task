<?php

use yii\db\Migration;

/**
 * Class m200911_221817_create_calculation_grafics_tables
 */
class m200911_221817_create_calculation_grafics_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%calculation_grafics}}', [
            'id' => $this->primaryKey(),
            'calculation_id' => $this->integer(),
            'pay_date' => $this->text()->Null(),
            'amount' => $this->integer(),
            'percent_amount' => $this->integer()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%calculation_grafics}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200911_221817_create_calculation_grafics_tables cannot be reverted.\n";

        return false;
    }
    */
}
