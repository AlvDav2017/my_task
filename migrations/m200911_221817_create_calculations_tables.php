<?php

use yii\db\Migration;

/**
 * Class m200911_221817_create_calculations_tables
 */
class m200911_221817_create_calculations_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%calculations}}', [
            'id' => $this->primaryKey(),
            'start_date' => $this->text()->Null(),
            'amount' => $this->integer(),
            'time' => $this->integer(),
            'percent' => $this->integer()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%calculations}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200911_221817_create_calculations_tables cannot be reverted.\n";

        return false;
    }
    */
}
