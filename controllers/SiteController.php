<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Calculations;
use app\models\CalculationGrafics;
use DateTime;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new Calculations();
        $date = $date1 = new DateTime();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $year = $date1->modify("+".$model->time." month");
            $employment_year = $year->format("m/d/Y");
            $diff1 = (new DateTime($employment_year))->diff(new DateTime());
            $day_year = $diff1->days+1;
            $amount = $model->amount/$model->time;

            for ($i=1; $i <= $model->time ; $i++) {
                $grafic = new CalculationGrafics();
                $date->modify("+".$i." month");

                $employment_date = $date->format("m/d/Y");
                $diff = (new DateTime($employment_date))->diff(new DateTime());
                $day_count = $diff->d+1;
                $h=$day_year*$day_count;
                $grafic->calculation_id = $model->id;
                $grafic->pay_date = $employment_date;
                $grafic->amount = $amount;
                $grafic->percent_amount = $model->amount*$model->percent/$h;
                $grafic->save();
            }

            return $this->redirect(['view','id' => $model->id]);

        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }
    /**
     * View action.
     *
     * @return Response|string
     */
    public function actionView($id)
    {
        $datas = CalculationGrafics::find()->where(['calculation_id'=>$id])->all();
        return $this->render('view', [
            'datas' => $datas
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
