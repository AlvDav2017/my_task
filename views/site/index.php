<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\Calculations */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'My Yii Application';

?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Простой калькулятор графика расчета!</h1>
    </div>
    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <?php $form = ActiveForm::begin(['id' => 'calculate-form']); ?>

                    <?= $form->field($model, 'start_date')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99/99/9999']); ?>

                    <?= $form->field($model, 'amount') ?>

                    <?= $form->field($model, 'time') ?>

                    <?= $form->field($model, 'percent') ?>

                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-success', 'name' => 'calculate-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
