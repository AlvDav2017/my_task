<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\Calculations */

use yii\helpers\Html;

$this->title = 'My Yii Application';

?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Простой калькулятор графика расчета!</h1>
    </div>
    <div class="body-content">
        <table class="table">
            <thead>
              <tr>
                <th>Номер платежа</th>
                <th>Дату платежа</th>
                <th>Общую сумма платежа (ежемесячный платеж)</th>
                <th>Сумму погашаемых процентов</th>
                <th>Сумму погашаемого основного долга</th>
                <th>Остаток основного долга по займу на текущую дату (дату платежа)</th>
              </tr>
            </thead>
            <tbody>
                <?php foreach ($datas as $data): ?>
                    <tr>
                      <td><?=$data->id?></td>
                      <td><?=$data->pay_date?></td>
                      <td><?=$data->amount+$data->percent_amount?></td>
                      <td><?=$data->percent_amount?></td>
                      <td><?=$data->amount?></td>
                      <td><?=$data->getPrice()?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
          </table>

    </div>
</div>
