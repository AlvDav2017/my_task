<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calculations".
 *
 * @property int $id
 * @property string $start_date
 * @property int $amount
 * @property int $time
 * @property int $percent
 */
class Calculations extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calculations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start_date', 'amount', 'time', 'percent'], 'required'],
            [[ 'amount', 'time', 'percent'], 'integer'],
            ['start_date', 'date','format' => 'php:m/d/Y', 'min' => date("m/d/Y")]

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'start_date' => Yii::t('models', 'Начальная дата'),
            'amount' => Yii::t('models', 'Сумма займа'),
            'time' => Yii::t('models', 'Срок займа (в месяцах)'),
            'percent' => Yii::t('models', 'Годовая процентная ставка'),
        ];
    }
}
