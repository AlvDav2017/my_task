<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calculation_grafics".
 *
 * @property int $id
 * @property string $start_date
 * @property int $amount
 * @property int $time
 * @property int $percent
 */
class CalculationGrafics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calculation_grafics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['calculation_id', 'amount', 'percent_amount'], 'required'],
            ['pay_date', 'safe']

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'calculation_id' => Yii::t('models', 'Номер платежа'),
            'pay_date' => Yii::t('models', 'Дату платежа'),
            'percent_amount' => Yii::t('models', 'Сумму погашаемых процентов'),
            'amount' => Yii::t('models', 'Сумму погашаемого основного долга'),
        ];
    }

    // ostatok
    public function getPrice()
    {
        $amount = 0;
        $datas = CalculationGrafics::find()->where(['>','id',$this->id])->all();
        foreach ($datas as $data) {
            $amount+=$data->percent_amount+$data->amount;
        }
        return $amount;
    }
}
